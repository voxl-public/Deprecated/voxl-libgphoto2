FROM arm64v8/ubuntu:bionic

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update

# Install libgphoto2 dependencies
RUN apt-get install -y apt-utils
RUN apt-get install -y libltdl-dev
RUN apt-get install -y pkg-config
RUN apt-get install -y libusb-1.0-0-dev
RUN apt-get install -y gettext
RUN apt-get install -y gtk-doc-tools
RUN apt-get install -y libexif-dev
RUN apt-get install -y usbutils
RUN apt-get install -y dh-autoreconf
RUN apt-get install -y libpopt-dev
RUN apt-get install -y udev
RUN apt-get install -y vim
RUN apt-get install -y libomxil-bellagio-dev
RUN apt-get install -y yasm libvpx. libx264.

# Download and install gphoto2 and libgphoto2
COPY gphoto2-updater.sh .
RUN chmod +x gphoto2-updater.sh
RUN ./gphoto2-updater.sh -s

RUN apt-get install -y python-pip
RUN pip install -v gphoto2

# Add voxl-libgphoto2 scripts
COPY voxl-libgphoto2.py .
COPY voxl-libgphoto2 .

# ffmpeg Installation
RUN git clone https://github.com/FFmpeg/FFmpeg.git
WORKDIR "/FFmpeg" 
RUN ./configure --arch=aarch64 --enable-omx --enable-gpl --enable-libx264
RUN make -j$(nproc)
RUN make install

WORKDIR "/"
