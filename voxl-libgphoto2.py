from __future__ import print_function

import io
import os
import subprocess as sp
import sys
import time
import re

import gphoto2 as gp

def is_ip_address(ip_addr):
    reg_ex = '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$'
    
    if not re.search(reg_ex, ip_addr):
        print("Invalid IP address:", ip_addr)
        return False
    else:
        return True

def usage():
    print("Usage:")
    print("\n./voxl-libgphoto2 HOST_IP")
    print("\nHOST_IP is the IP address of the host PC running QGC.")


def main():
    if len(sys.argv) < 2:
        usage()
        exit(1)
        
    ip_addr = str(sys.argv[1])

    if not is_ip_address(ip_addr):
        usage()
        exit(1)

    frames = 0
    camera = gp.check_result(gp.gp_camera_new())
    start_time = time.time()

    ffmpeg_command = 'ffmpeg -i - -c:v libx264 -f mpegts udp://'+ip_addr+':4242'

    pipe = sp.Popen(ffmpeg_command.split(), stdin=sp.PIPE)

    print("\nPress CTRL-C to kill stream.\n")
    try:
        try:
            while True:
                try:
                    camera_file = gp.check_result(gp.gp_camera_capture_preview(camera))
                except:
                    # Catch camera data not found error
                    print("\nFailed to read camera data.\nTry changing camera mode then changing back to original mode.")
                    exit(1)
                file_data = gp.check_result(gp.gp_file_get_data_and_size(camera_file))

                pipe.stdin.write(io.BytesIO(file_data).read())

                # flush output here to force SIGPIPE to be triggered
                # while inside this try block.
                sys.stdout.flush()

                frames += 1
        except IOError:
            # Python flushes standard streams on exit; redirect remaining output
            # to devnull to avoid another IOError at shutdown
            print("\nIOERROR detected!")
            devnull = os.open(os.devnull, os.O_WRONLY)
            os.dup2(devnull, sys.stdout.fileno())
            sys.exit(1)  # Python exits with error code 1 on EPIPE

    except KeyboardInterrupt:
        pass

    end_time = time.time()
    total_time = end_time - start_time
    print("\nFrames captured:", frames)
    print("Elapsed time:", total_time)
    print("Average fps:", frames / total_time)

    gp.check_result(gp.gp_camera_exit(camera))

    return 0


if __name__ == "__main__":
    sys.exit(main())
