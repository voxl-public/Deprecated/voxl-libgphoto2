# voxl-libgphoto2

voxl-libgphoto2 enables streaming a live video feed from a wide range of digital cameras to QGroundControl. The following software is used to make this possible.

[gPhoto2](http://www.gphoto.org/) is a free, redistributable, ready to use set of digital camera software applications for Unix-like systems, written by a whole team of dedicated volunteers around the world. It supports more than [2500 cameras](http://www.gphoto.org/proj/libgphoto2/support.php)

[libgphoto2](http://www.gphoto.org/proj/libgphoto2/) is the core library designed to allow access to digital camera by external programs.

[FFmpeg](https://ffmpeg.org/) is a complete, cross-platform solution to record, convert and stream audio and video. FFmpeg has a large number of options that can be used to tune the video stream to your liking. Among the default encoders, voxl-libgphoto2 also supports software encoding using [libx264](https://trac.ffmpeg.org/wiki/Encode/H.264) and hardware encoding using [OpenMax](https://trac.ffmpeg.org/wiki/HWAccelIntro)

## Push files to target

```
$ ./push-files-to-target
```

This script creates the necessary files needed to build the libgphoto2 docker image on VOXL.

## Build docker image

On VOXL:

```
# Enable station mode in order to install dependencies
$ voxl-wifi station [ssid] [password]
$ reboot

$ cd /data/docker

$ docker build -t libgphoto2 libgphoto2/
```

## Run container

```
$ docker run -i -t --rm -v /dev/bus/usb/:/dev/bus/usb --net=host --privileged libgphoto2
```

## Using the gphoto2 command line interface

The [gPhoto docs](http://www.gphoto.org/doc/manual/using-gphoto2.html) provide a good explanation on how to use the gPhoto command line interface

Here are the first few examples:

```
$ gphoto2 --list-ports
Devices found: 4
Path                             Description
--------------------------------------------------------------
ptpip:                           PTP/IP Connection               
ip:                              IP Connection                   
serial:                          Serial Port Device              
usb:001,005                      Universal Serial Bus
```

In this example we can see that gPhoto has detected a USB bus on the device. However, we cannot tell yet whether we will have write access to the USB device file your camera will be assigned by the operating system.

The next step is to connect your camera and find out whether the gphoto2 can find it. This only works with USB. 

```
$ gphoto2 --auto-detect
Model                          Port                                            
----------------------------------------------------------
Canon EOS 700D                 usb:001,005 
```

In this case, a camera called “Canon EOS 700D” is connected to your system's USB bus.


```
$ gphoto2 --capture-image-and-download
New file is in location /capt0000.cr2 on the camera                            
Saving file as capt0000.cr2
Deleting file /capt0000.cr2 on the camera
```

This command can be used to take a picture and immediately save the image to the host docker container. The image format will depend on the camera.

## Streaming video using voxl-libgphoto2
In order to stream a video feed from the digital camera to QGroundControl, use the following:


```
$ ./voxl-libgphoto2 192.168.8.XXX
```

The referenced IP address should be the IP address of the host PC running QGroundControl.

- On the host system, while connected to the VOXL's Wi-Fi, run QGroundControl

- Next, in QGroundControl, press the purple QGC logo in the top left corner in order to Access the `Application Settings` menu.

- Under the General tab, scroll down until you find the `Video` section.

- Under the `Video Source` dropdown, choose `MPEG-TS (h.264) Video Stream`

- In the `UDP Port` field. enter the default: `4242`

- You will now be able to view the video stream under QGroundControl’s `Fly` view.

## Customizing FFmpeg parameters

By default, voxl-libgphoto2 uses the following basic FFmpeg parameters, specified in the [voxl-libgphoto2.py script](https://gitlab.com/voxl-public/voxl-libgphoto2/-/blob/master/voxl-libgphoto2.py#L42):

```
ffmpeg -i - -c:v libx264 -f mpegts udp://192.168.8.XXX:4242
```

These parameters can be modified to change the stream's encoder, resolution, bitrate, framerate, etc. as needed.

## ModalAI Tested Cameras

The following cameras were tested by ModalAI and successfully used to stream video to QGroundControl:

- Canon EOS 700D
- Canon PowerShot G7 X Mark III

